import { watch } from "vue";

export function useWatchCharacters(inputValue, limit) {
  watch(inputValue, newValue => {
    if (newValue.length === limit) {
      alert("you reach the limit...");
    }
  });
}
