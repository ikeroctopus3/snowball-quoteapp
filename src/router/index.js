import { createRouter, createWebHistory } from "vue-router";
import ViewNotes from "../views/ViewNotes.vue";
import MyEditionView from "../views/MyEditionView.vue";
import AboutView from "../views/AboutView.vue";
import StatsView from "../views/StatsView.vue";
import EditNote from "../views/EditNote.vue";
import AddShowNotes from "../views/AddShowNotes.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "notes",
      component: ViewNotes,
    },
    {
      path: "/add-show-notes",
      name: "show-notes",
      component: AddShowNotes,
    },
    {
      path: "/edit-note/:id",
      name: "edit-note",
      component: EditNote,
    },
    {
      path: "/stats",
      name: "stats",
      component: StatsView,
    },
    {
      path: "/my-edition",
      name: "my-edition",
      component: MyEditionView,
    },
    {
      path: "/about",
      name: "about",
      component: AboutView,
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
