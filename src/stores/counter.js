// import { ref, computed } from "vue";
import { defineStore } from "pinia";

// export const useCounterStore = defineStore("counter", () => {
//   const count = ref(0);
//   const title = ref("My Title from pinia");
//   const doubleCount = computed(() => count.value * 2);
//   function increment() {
//     count.value++;
//   }

//   return { count, title, doubleCount, increment };
// });

export const useStoreNotes = defineStore({
  id: "storeNotes",
  state: () => ({
    count: 0,
    title: "My new syntax from pinia",
    notes: [
      { id: "1", content: "The only way to do great work is to love what you do.", author: "Steve Jobs" },
      {
        id: "2",
        content: "In three words I can sum up everything I've learned about life: it goes on.",
        author: "Robert Frost",
      },
      {
        id: "3",
        content: "The best time to plant a tree was 20 years ago. The second best time is now.",
        author: "Chinese Proverb",
      },
      { id: "4", content: "The only thing we have to fear is fear itself.", author: "Franklin D. Roosevelt" },
      {
        id: "5",
        content: "Success is not final, failure is not fatal: It is the courage to continue that counts.",
        author: "Winston Churchill",
      },
      { id: "6", content: "Life is what happens when you're busy making other plans.", author: "John Lennon" },
      { id: "7", content: "The journey of a thousand miles begins with a single step.", author: "Lao Tzu" },
      {
        id: "8",
        content:
          "To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment.",
        author: "Ralph Waldo Emerson",
      },
      {
        id: "9",
        content: "The greatest glory in living lies not in never falling, but in rising every time we fall.",
        author: "Nelson Mandela",
      },
      {
        id: "10",
        content: "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.",
        author: "Albert Einstein",
      },
    ],
  }),
  actions: {
    increment() {
      this.count++;
    },
    addNote(newContent, newAuthor) {
      //generate id
      const id = Math.random().toString(16).slice(3);
      let newNote = { id: id, content: newContent, author: newAuthor };
      this.notes.push(newNote);
    },
    deleteQuote(id) {
      this.notes = this.notes.filter(note => note.id !== id);
    },
    updateNote(id, content, author) {
      const indexOfNote = this.notes.findIndex(note => note.id === id);
      this.notes[indexOfNote].content = content;
      this.notes[indexOfNote].author = author;
    },
  },
  getters: {
    doubleCount() {
      return this.count * 2;
    },
    totalNotes(state) {
      // return this.notes.length;
      return state.notes.length;
    },
    totalCharacters(state) {
      let counter = 0;
      for (let note of state.notes) {
        counter += note.content.length;
      }
      return counter;
    },
  },
});
